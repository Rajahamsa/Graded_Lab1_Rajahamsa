package com.performers;

import com.performers.bean.PerformerBean;

public class Vocalists implements IPerformers {

	String Key;
	int Volume;

	public Vocalists() {
		super();
	}

	public Vocalists(String key, int volume) {
		Key = key;
		Volume = volume;
	}

	public String getKey() {
		return Key;
	}

	public void setKey(String key) {
		Key = key;
	}

	public int getVolume() {
		return Volume;
	}

	public void setVolume(int volume) {
		Volume = volume;
	}

	public String perform(PerformerBean pb) {
		return "I sing in the key of –" + getKey() + " –at the volume" + getVolume() + "-" + pb.getId();
	}

}
