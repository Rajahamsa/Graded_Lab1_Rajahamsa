package com.performers;

import java.util.ArrayList;
import java.util.List;

import com.performers.bean.PerformerBean;

public class UniqueId {

	IPerformers i;
	String id;

	static List<PerformerBean> list = new ArrayList<PerformerBean>();

	public static String checkId(PerformerBean pb) {
		if (!list.contains(pb)) {
			list.add(pb);
		} else {
			throw new IllegalArgumentException("Id must be unique");
		}

		return pb.getId();
	}

}
