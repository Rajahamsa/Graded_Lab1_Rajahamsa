package com.performers;

import com.performers.bean.PerformerBean;

public class Dancers implements IPerformers {

	String Style;

	public Dancers() {

	}

	public String getStyle() {
		return Style;
	}

	public void setStyle(String style) {
		Style = style;
	}

	public Dancers(String style) {
		Style = style;
	}

	public String perform(PerformerBean pb) {

		return getStyle() + "-" + pb.getId() + "-" + "dancer";
	}

}
