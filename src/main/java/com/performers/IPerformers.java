package com.performers;

import com.performers.bean.PerformerBean;

public interface IPerformers {

	public String perform(PerformerBean pb);

}
