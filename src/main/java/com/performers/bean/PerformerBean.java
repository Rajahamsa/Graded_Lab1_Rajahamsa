package com.performers.bean;

public class PerformerBean {
	String id;
	boolean dancer;
	boolean performer;
	boolean vocalist;

	public PerformerBean(String id, boolean dancer, boolean performer, boolean vocalist) {
		this.id = id;
		this.dancer = dancer;
		this.performer = performer;
		this.vocalist = vocalist;
	}

	public PerformerBean() {
		super();
	}

	public PerformerBean(String id) {
		this.id = id;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public boolean isDancer() {
		return dancer;
	}

	public void setDancer(boolean dancer) {
		this.dancer = dancer;
	}

	public boolean isPerformer() {
		return performer;
	}

	public void setPerformer(boolean performer) {
		this.performer = performer;
	}

	public boolean isVocalist() {
		return vocalist;
	}

	public void setVocalist(boolean vocalist) {
		this.vocalist = vocalist;
	}

	@Override
	public String toString() {
		return "PerformerBean [id=" + id + "]";
	}

}
