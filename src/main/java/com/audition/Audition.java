package com.audition;


import java.util.ArrayList;
import java.util.List;

import com.performers.Dancers;
import com.performers.IPerformers;
import com.performers.Performers;
import com.performers.UniqueId;
import com.performers.Vocalists;
import com.performers.bean.PerformerBean;

public class Audition {

	public static void main(String[] args) {
		IPerformers i = new Performers();
		List<PerformerBean> list = new ArrayList<PerformerBean>();
		list.add(new PerformerBean("per4"));
		list.add(new PerformerBean("per5"));
		list.add(new PerformerBean("per6"));
		list.add(new PerformerBean("per7"));

		for (PerformerBean pb1 : list) {
			UniqueId.checkId(pb1);
			System.out.println(i.perform(pb1));
		}
		
		Dancers d1=new Dancers() ;
		PerformerBean pb2=new PerformerBean("dan1");
		d1.setStyle("tap");
		System.out.println(d1.perform(pb2));
		
		Dancers d2=new Dancers() ;
		PerformerBean pb3=new PerformerBean("dan2");
		d2.setStyle("rock");
		System.out.println(d2.perform(pb3));
		
		Dancers d3=new Dancers() ;
		PerformerBean pb4=new PerformerBean("dan3");
		d3.setStyle("jazz");
		System.out.println(d3.perform(pb4));
		
		Vocalists v1=new Vocalists() ;
		PerformerBean pb5=new PerformerBean("vocal3");
		v1.setKey("G");
		v1.setVolume(5);
		System.out.println(v1.perform(pb5));
		
		
		
	}
}
