package com.performers;


import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import com.performers.IPerformers;
import com.performers.UniqueId;
import com.performers.bean.PerformerBean;

public class IPerformerTest {
	
	IPerformers i=new Performers();
	PerformerBean pb=new PerformerBean();
	
	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testCheckforUniqueId() {
		
		String ExpectedisUnique="s198";
		pb.setId("s198");
		String ActualisUnique=UniqueId.checkId(pb);
		assertEquals(ExpectedisUnique,ActualisUnique);
		
		
	}

}
