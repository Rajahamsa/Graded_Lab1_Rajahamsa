package com.performers;



import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

import com.performers.bean.PerformerBean;

public class DancersTest {
	
	Dancers d;
	PerformerBean pb;

	@Before
	public void setUp()  {
		d=new Dancers();
	}

	@Test
	public void testDancersPerform() {
		d.setStyle("tap");
		pb=new PerformerBean("dan4");
		String expected="tap-dan4-dancer";
		String actual=d.perform(pb);
		assertEquals(expected,actual);
		
	}

}
