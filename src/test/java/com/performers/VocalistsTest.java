package com.performers;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import com.performers.bean.PerformerBean;

public class VocalistsTest {

	Vocalists v;
	PerformerBean pb;

	@Before
	public void setUp()  {
		v=new Vocalists();
	}

	@Test
	public void testDancersPerform() {
		v.setKey("G");
		v.setVolume(5);
		pb=new PerformerBean("vocal4");
		String expected="I sing in the key of –G –at the volume5-vocal4";
		String actual=v.perform(pb);
		assertEquals(expected,actual);
		
	}


}
