package com.performers;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import com.performers.bean.PerformerBean;

public class PerformersTest {
	
	IPerformers i=new Performers();
	PerformerBean pb=new PerformerBean();
	
   
	@Before
	public void setUp() {
		pb.setId("xss99");
		
	}

	@Test
	public void testDisplay() {
		String expected="xss99-performer";
		UniqueId.checkId(pb);
		String actual =i.perform(pb);
		assertEquals(expected,actual);
	}

}
